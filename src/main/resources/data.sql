delete from gift_certificate;

insert into gift_certificate(name, description, price, duration, create_date) 
values('gc1', 'gift certificate one', 10.5, 10, '2021-07-04 22:46:00');

insert into gift_certificate(name, description, price, duration, create_date) 
values('gc2', 'gift certificate two', 12.5, 20, '2021-07-03 22:46:00');

insert into gift_certificate(name, description, price, duration, create_date) 
values('gc3', 'gift certificate three', 13.5, 30, '2021-07-02 22:46:00');

insert into gift_certificate(name, description, price, duration, create_date) 
values('gc4', 'gift certificate four', 14.5, 40, '2021-07-01 22:46:00');

insert into gift_certificate(name, description, price, duration, create_date) 
values('gc5', 'gift certificate five', 15.5, 50, '2021-06-30 22:46:00');

delete from tag;

insert into tag(name) values('dell');

insert into tag(name) values('lenovo');

insert into tag( name) values('apple');

insert into tag(name) values('samsung');

insert into tag(name) values('asus');



