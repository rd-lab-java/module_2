package com.epam.esm.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.epam.esm.repository.entity.GiftCertificate;

public class GiftCertificateMapper implements RowMapper<GiftCertificate> {

	@Override
	public GiftCertificate mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		GiftCertificate giftCertificate = new GiftCertificate();
		giftCertificate.setId(rs.getLong("id"));
		giftCertificate.setName(rs.getString("name"));
		giftCertificate.setDescription(rs.getString("description"));
		giftCertificate.setPrice(rs.getDouble("price"));
		giftCertificate.setDuration(rs.getInt("duration"));
		giftCertificate.setCreatedDate(rs.getTimestamp("create_date"));
		giftCertificate.setUpdatedDate(rs.getTimestamp("last_update_date"));
		return giftCertificate;
	}

}
