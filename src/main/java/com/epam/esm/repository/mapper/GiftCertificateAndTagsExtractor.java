package com.epam.esm.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
import com.epam.esm.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.service.exception.TagNotFoundException;

public class GiftCertificateAndTagsExtractor implements ResultSetExtractor<List<GiftCertificate>> {

	private TagRepository tagRepository;
	private Environment environment;
	
	public GiftCertificateAndTagsExtractor(TagRepository tagRepository, Environment environment) {
		super();
		this.tagRepository = tagRepository;
		this.environment = environment;
	}
	
	@Override
	public List<GiftCertificate> extractData(ResultSet rs) throws SQLException, DataAccessException {
		List<GiftCertificate> giftCertificates = null;
		while(rs.next()) {
			GiftCertificate giftCertificate = new GiftCertificate();
			giftCertificate.setId(rs.getLong("id"));
			giftCertificate.setName(rs.getString("name"));
			giftCertificate.setDescription(rs.getString("description"));
			giftCertificate.setPrice(rs.getDouble("price"));
			giftCertificate.setDuration(rs.getInt("duration"));
			giftCertificate.setCreatedDate(rs.getTimestamp("create_date"));
			giftCertificate.setUpdatedDate(rs.getTimestamp("last_update_date"));
			
			List<Tag> tags = tagRepository.findTagsByGiftCertificateId(giftCertificate.getId());			
			giftCertificate.setTags(tags); 
			
			if(giftCertificates == null)
				giftCertificates = new ArrayList<>();
			
			giftCertificates.add(giftCertificate);
		}
		
		if(giftCertificates == null)
			throw new GiftCertificateNotFoundException(environment.getProperty("giftcertificate.searchnotfound.message"));
		return giftCertificates;
	}
	
}
