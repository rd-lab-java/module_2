package com.epam.esm.repository;

import java.util.List;

import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
/**
 * Gift Certificate repository interface 
 * imposes basic operations that working with data source.
 * @author yusuf
 *
 */
public interface GiftCertRepository {
	GiftCertificate saveGiftCertificate(GiftCertificate giftCertificate);
	List<GiftCertificate> findAll();
	GiftCertificate findGiftCertificateById(long id);
	GiftCertificate updateGiftCertificateById(long id, GiftCertificate giftCertificate);
	GiftCertificate deleteGiftCertificateById(long id);
	boolean saveGiftCertificateAndTag(GiftCertificate giftCertificate, Tag tag);
	List<GiftCertificate> findSortedGiftCertificatesByTagName(String tagName, String sort);
	List<GiftCertificate> findGiftCertificatesByTagName(String tagName);
	List<GiftCertificate> findSortedGiftCertificatesByPartOfName(String partOfName, String sort);
	List<GiftCertificate> findGiftCertificatesByPartOfName(String partOfName);
	List<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfDesc(String partOfDesc, String sort);
	List<GiftCertificate> findGiftCertificatesByPartOfDesc(String partOfDesc);
	/* tear down process*/
	void deleteAll();
}
