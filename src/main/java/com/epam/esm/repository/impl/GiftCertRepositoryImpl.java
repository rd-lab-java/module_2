package com.epam.esm.repository.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.epam.esm.repository.GiftCertRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
import com.epam.esm.repository.mapper.GiftCertificateAndTagsExtractor;
import com.epam.esm.repository.mapper.GiftCertificateMapper;
import com.epam.esm.repository.mapper.TagMapper;
import com.epam.esm.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.service.exception.GiftCertificateNotModifiedException;
import com.epam.esm.service.exception.TagDuplicateException;
/**
 * Gift Certificate repository implementation
 * contains implementation with data source 
 * and has jdbc template {@link JdbcTemplate} dependency and
 * tag repository {@link TagRepository} dependency
 * 
 * @author yusuf
 *
 */
@Repository
@PropertySources({
		@PropertySource(name="sqlStatements", value = "classpath:sql.statements"),
		@PropertySource(name="stringValues", value = "classpath:string.values")
})
public class GiftCertRepositoryImpl implements GiftCertRepository {

	private Environment environment;
	private JdbcTemplate jdbcTemplate;
	private TagRepository tagRepository;
	
	
	@Autowired
	public GiftCertRepositoryImpl(Environment environment, JdbcTemplate jdbcTemplate, TagRepository tagRepository) {
		super();
		this.environment = environment;
		this.jdbcTemplate = jdbcTemplate;
		this.tagRepository = tagRepository;
	}


	@Override
	public GiftCertificate saveGiftCertificate(GiftCertificate giftCertificate) {
		
		try {
			KeyHolder keyHolder = new GeneratedKeyHolder();
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(environment.getProperty("giftcertificate.insert_gift_certificate"),
						Statement.RETURN_GENERATED_KEYS);
				
				ps.setString(1, giftCertificate.getName());
				ps.setString(2, giftCertificate.getDescription());
				ps.setDouble(3, giftCertificate.getPrice());
				ps.setInt(4, giftCertificate.getDuration());
				ps.setObject(5, Timestamp.valueOf(LocalDateTime.now()));
				ps.setObject(6, giftCertificate.getUpdatedDate());
				return ps;
			}, keyHolder);
			giftCertificate.setId(keyHolder.getKey().longValue());
			saveGiftCertificateAndTags(giftCertificate);
			return giftCertificate;
		} catch(DuplicateKeyException ex) {
			throw new GiftCertificateDuplicateException(String.format(environment.getProperty("giftcertificate.duplicate.message"), 
					giftCertificate.getName()), ex);
		}
		
	}
	
	
	/**
	 * save if the there are tags with certificate
	 * save only new tags for the certificate.
	 * 
	 * @param giftCertificate
	 * @return true if successfully save 
	 */
	private boolean saveGiftCertificateAndTags(GiftCertificate giftCertificate) {
		long savedItemCount = giftCertificate.getTags().stream().filter(new Predicate<Tag>() {
			@Override
			public boolean test(Tag tag) {
				tag.setId(saveAndGetTag(tag).getId());
				return saveGiftCertificateAndTag(giftCertificate, tag);
			}
		}).count();
		return savedItemCount > 0;
	}
	
	/**
	 * save new tag or get existing one if occurs overwriting 
	 * 
	 * @param tag
	 * @return saved tag, or existing tag
	 */
	private Tag saveAndGetTag(Tag tag) {
		try {
			return tagRepository.saveTag(tag);
		} catch(TagDuplicateException ex) {
			return tagRepository.findTagByName(tag.getName());
		}
	}

	@Override
	public List<GiftCertificate> findAll() {
		List<GiftCertificate> giftCertificates = jdbcTemplate.query(environment.getProperty("giftcertificate.find_all")
				, new GiftCertificateMapper());
		if(giftCertificates == null || giftCertificates.size() == 0)
			throw new GiftCertificateNotFoundException(environment.getProperty("giftcertificate.allnotfound.message"));
		return giftCertificates;
	}

	@Override
	public GiftCertificate findGiftCertificateById(long id) {
		try {
			GiftCertificate giftCertificate = jdbcTemplate.queryForObject(
					environment.getProperty("giftcertificate.find_gift_certificate"), new GiftCertificateMapper(), id);
			return giftCertificate;
		} catch(EmptyResultDataAccessException ex) {
			throw new GiftCertificateNotFoundException(String.format(environment.getProperty("giftcertificate.notfound.message"), id), ex);
		}
	}

	@Override
	public GiftCertificate updateGiftCertificateById(long id, GiftCertificate giftCertificate) {
		try {
			GiftCertificate existedGiftCertificate = jdbcTemplate.queryForObject(
					environment.getProperty("giftcertificate.find_gift_certificate"), new GiftCertificateMapper(), id);
			
			giftCertificate.setId(id);
			giftCertificate.setUpdatedDate(new Date());
			giftCertificate.setCreatedDate(existedGiftCertificate.getCreatedDate());
			
			String updatingFileds = existedGiftCertificate.getFieldsToUpdate(giftCertificate);
			boolean isNewTagAdded = saveGiftCertificateAndTags(giftCertificate);
			
			if(updatingFileds.length() == 0 && !isNewTagAdded) {
				throw new GiftCertificateNotModifiedException(String.format(environment.getProperty("giftcertificate.notmodified.message"), id));
			}
			
			jdbcTemplate.update(String.format(environment.getProperty("giftcertificate.update_gift_certificate"), updatingFileds), 
					Timestamp.valueOf(LocalDateTime.now()), giftCertificate.getId());
			
			return giftCertificate;
		} catch(EmptyResultDataAccessException ex) {
			throw new GiftCertificateNotFoundException(String.format(environment.getProperty("giftcertificate.notfound.message"), id), ex);
		}
	}

	@Override
	public GiftCertificate deleteGiftCertificateById(long id) {
		try {
			GiftCertificate existedGiftCertificate = jdbcTemplate.queryForObject(
					environment.getProperty("giftcertificate.find_gift_certificate"), new GiftCertificateMapper(), id);
			jdbcTemplate.update(environment.getProperty("giftcertificate.delete_gift_certificate"), id);
			return existedGiftCertificate;
		} catch(EmptyResultDataAccessException ex) {
			throw new GiftCertificateNotFoundException(String.format(environment.getProperty("giftcertificate.notfound.message"), id), ex);
		}
	}

	@Override
	public boolean saveGiftCertificateAndTag(GiftCertificate giftCertificate, Tag tag) {
		return jdbcTemplate.update(environment.getProperty("giftcertificate.insert_gift_cert_tag"), giftCertificate.getId(), tag.getId()) > 0;
	}

	@Override
	public List<GiftCertificate> findSortedGiftCertificatesByTagName(String tagName, String sort) {
		return jdbcTemplate.query(String.format(environment.getProperty("giftcertificate.find_sorted_gift_certificates_tags_by_tag_name"),
				sort), new GiftCertificateAndTagsExtractor(tagRepository, environment), new Object[] {tagName});
	}

	@Override
	public List<GiftCertificate> findGiftCertificatesByTagName(String tagName) {
		return jdbcTemplate.query(environment.getProperty("giftcertificate.find_gift_certificates_tags_by_tag_name"), 
				new GiftCertificateAndTagsExtractor(tagRepository, environment), new Object[] {tagName});
	}

	@Override
	public List<GiftCertificate> findSortedGiftCertificatesByPartOfName(String partOfName, String sort) {
		return jdbcTemplate.query(String.format(environment.getProperty("giftcertificate.find_sorted_gift_certificates_tags_by_part_of_name"),
				sort), new GiftCertificateAndTagsExtractor(tagRepository, environment), new Object[] {"%" + partOfName + "%"});
	}

	@Override
	public List<GiftCertificate> findGiftCertificatesByPartOfName(String partOfName) {
		return jdbcTemplate.query(environment.getProperty("giftcertificate.find_gift_certificates_tags_by_part_of_name"), 
				new GiftCertificateAndTagsExtractor(tagRepository, environment), new Object[] {"%" +partOfName + "%"});
	}

	@Override
	public List<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfDesc(String partOfDesc, String sort) {
		return jdbcTemplate.query(String.format(environment.getProperty("giftcertificate.find_sorted_gift_certificates_tags_by_part_of_desc"), 
				sort), new GiftCertificateAndTagsExtractor(tagRepository, environment), new Object[] {"%" + partOfDesc + "%"});
	}

	@Override
	public List<GiftCertificate> findGiftCertificatesByPartOfDesc(String partOfDesc) {
		return jdbcTemplate.query(environment.getProperty("giftcertificate.find_gift_certificates_tags_by_part_of_desc"), 
				new GiftCertificateAndTagsExtractor(tagRepository, environment), new Object[] {"%" +partOfDesc + "%"});
	}


	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		jdbcTemplate.update(environment.getProperty("giftcertificate.delete_all"));
	
		/** unset when using embedded h2 database */
		jdbcTemplate.update(environment.getProperty("giftcertificate.resetautoincrement"));
		tagRepository.deleteAll();
	}
}
