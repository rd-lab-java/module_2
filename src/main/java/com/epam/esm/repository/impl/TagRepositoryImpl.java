package com.epam.esm.repository.impl;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
import com.epam.esm.repository.mapper.GiftCertificateMapper;
import com.epam.esm.repository.mapper.TagMapper;
import com.epam.esm.service.exception.TagDuplicateException;
import com.epam.esm.service.exception.TagNotFoundException;
/**
 * Tag repository implementation contains
 * implementations with data source 
 * and has jdbc template {@link JdbcTemplate} dependency
 * 
 * @author yusuf
 *
 */
@Repository
@PropertySources({
	@PropertySource(name="sqlStatements", value = "classpath:sql.statements"),
	@PropertySource(name="stringValues", value = "classpath:string.values")
})
public class TagRepositoryImpl implements TagRepository {
	
	private JdbcTemplate jdbcTemplate;
	private Environment environment;
	
	
	@Autowired
	public TagRepositoryImpl(JdbcTemplate jdbcTemplate, Environment environment) {
		super();
		this.jdbcTemplate = jdbcTemplate;
		this.environment = environment;
	}

	@Override
	public Tag saveTag(Tag tag) {
		try {
			KeyHolder keyHolder = new GeneratedKeyHolder();
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(environment.getProperty("tag.insert_tag"), Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, tag.getName());
				return ps;
			}, keyHolder);
			tag.setId(keyHolder.getKey().longValue());
			return tag;
		} catch(DuplicateKeyException ex) {
			throw new TagDuplicateException(String.format(environment.getProperty("tag.duplicate.message"), tag.getName()), ex);
		}
	}

	@Override
	public List<Tag> findTagsByGiftCertificateId(long giftCertId) {
		return jdbcTemplate.query(environment.getProperty("tag.find_gc_tags"), new TagMapper(), new Object[] {giftCertId});
	}

	@Override
	public Tag findTagByName(String name) {
		try {
			return jdbcTemplate.queryForObject(environment.getProperty("tag.find_tag_by_name"), new TagMapper(), name);
		} catch(EmptyResultDataAccessException ex) {
			throw new TagNotFoundException(String.format(environment.getProperty("tag.notfoundbyname.message"), name), ex);
		}
	}

	@Override
	public List<Tag> findAll() {
		List<Tag> tags = jdbcTemplate.query(environment.getProperty("tag.find_all"), new TagMapper());
		if(tags == null || tags.size() == 0)
			throw new TagNotFoundException(environment.getProperty("tag.allnotfound.message"));
		return tags;		
	}

	@Override
	public Tag deleteTagById(long id) {
		try {
			Tag existedTag = jdbcTemplate.queryForObject(environment.getProperty("tag.find_tag_by_id"), new TagMapper(), id);
			jdbcTemplate.update(environment.getProperty("tag.delete_tag"), id);
			return existedTag;
		} catch(EmptyResultDataAccessException ex) {
			throw new TagNotFoundException(String.format(environment.getProperty("tag.notfoundbyid.message"), id), ex);
		}
	}

	@Override
	public Tag findTagById(long id) {
		try {
			return jdbcTemplate.queryForObject(environment.getProperty("tag.find_tag_by_id"), new TagMapper(), id);
		} catch(EmptyResultDataAccessException ex) {
			throw new TagNotFoundException(String.format(environment.getProperty("tag.notfoundbyid.message"), id), ex);
		}
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		jdbcTemplate.update(environment.getProperty("tag.delete_all"));
		
		/** unset when using h2 embedded database */
		jdbcTemplate.update(environment.getProperty("tag.resetautoincrement"));
	}
}
