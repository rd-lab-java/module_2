package com.epam.esm.repository;

import java.util.List;

import com.epam.esm.repository.entity.Tag;
/**
 * Tag repository interface
 * imposes basic CRUD operations 
 * that working directly with data source
 * 
 * @author yusuf
 *
 */
public interface TagRepository {

	Tag saveTag(Tag tag);
	Tag findTagByName(String name);
	List<Tag> findTagsByGiftCertificateId(long giftCertId);
	List<Tag> findAll();
	Tag deleteTagById(long id);
	Tag findTagById(long id);
	
	/* tear down process */
	void deleteAll();
}
