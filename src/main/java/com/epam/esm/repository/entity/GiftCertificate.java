package com.epam.esm.repository.entity;


import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
public class GiftCertificate {
    private long id;
    private String name;
    private String description;
    private double price;
    private int duration;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date createdDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date updatedDate;
	
	private Collection<Tag> tags = new ArrayList<>();

	public GiftCertificate(String name, String description, double price, int duration) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.duration = duration;
		createdDate = new Date();
	}
	
	public GiftCertificate() {
		createdDate = new Date();
	}
	
	public String getFieldsToUpdate(GiftCertificate giftCertificate) {
		StringBuilder part = new StringBuilder();
		
		if(!this.name.equals(giftCertificate.getName())) {
			part.append(String.format("name='%s',", giftCertificate.getName()));
		}
		
		if(this.description != null && !this.getDescription().equals(giftCertificate.getDescription())) {
			part.append(String.format("description='%s',", giftCertificate.getDescription()));
		}
		
		if(this.price != giftCertificate.getPrice()) {
			part.append(String.format("price='%s'," , giftCertificate.getPrice()));
		}
		
		if(this.duration != giftCertificate.getDuration()) {
			part.append(String.format("duration='%s',", giftCertificate.getDuration()));
		}
		
		return part.toString();
	}
}
