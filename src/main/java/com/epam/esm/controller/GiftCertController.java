package com.epam.esm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.service.GiftCertManager;
import com.epam.esm.service.TagManager;
import com.epam.esm.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.service.exception.GiftCertificateNotModifiedException;
import com.epam.esm.util.ResponseUtils;
/**
 * Rest controller api for managing 
 * gift certificate based requests and responses.
 * @author yusuf
 *
 */
@RestController
@RequestMapping("/giftcertificates")
public class GiftCertController {
	
	/** Gift Certificate Service class variable */
	@Autowired
	private GiftCertManager giftCertManager;
	
	/** Tag Service class variable */
	@Autowired
	private TagManager tagManager;
	
	/**
	 * Used to save new gift certificate, can contain zero or more tags
	 * 
	 * @param giftCertificate
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 * @throws Duplicate exception {@link GiftCertificateDuplicateException}
	 *  when trying to insert duplicate gift certificate
	 */
	@PostMapping(consumes = "application/json", produces = "application/json")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseUtils saveGiftCert(@RequestBody GiftCertificate giftCertificate) {
		return ResponseUtils.response(201, "Gift certificate created successfully", giftCertManager.saveGiftCertificate(giftCertificate));
	}
	
	/**
	 * Used to find all gift certificates
	 * 
	 * @return {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate}) without tags
	 * @throws Not found exception {@link GiftCertificateNotFoundException}
	 *  when no found any gift certificate 
	 */
	@GetMapping(produces = "application/json")
	public ResponseUtils findAll() {
		return ResponseUtils.success(giftCertManager.findAll());
	}
	
	/**
	 * Used to find gift certificate by gift certificate id
	 * 
	 * @param gift certificate id
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate}) without tags
	 * @throws Not found exception {@link GiftCertificateNotFoundException}
	 *  when no found any gift certificate based on id
	 */
	@GetMapping(path = "/{id:[0-9]+}", produces = "application/json")
	public ResponseUtils findGiftCertificate(@PathVariable long id) {
		return ResponseUtils.success(giftCertManager.findGiftCertificateById(id));
	}
	
	/**
	 * Used to update the only gift certificate's different fields,
	 * or tags that is new to the gift certificate
	 * 
	 * @param id of {@link GiftCertificate} that is to be updated
	 * @param {@link GiftCertificate} contains new values 
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 * @throws Not found exception {@link GiftCertificateNotFoundException}
	 * when no found gift certificate based id 
	 * or not modified exception {@link GiftCertificateNotModifiedException}
	 * when try to update unmodified gift certificate
	 */
	@PutMapping(path = "/{id:[0-9]+}", produces = "application/json", consumes = "application/json")
	public ResponseUtils updateGetCertificate(@PathVariable long id, @RequestBody GiftCertificate giftCertificate) {
		return ResponseUtils.success(giftCertManager.updateGiftCertificateById(id, giftCertificate));
	}
	
	/**
	 * Used to delete gift certificate 
	 * 
	 * @param {@link GiftCertificate} id 
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 * @throws Not found exception {@link GiftCertificateNotFoundException} 
	 * when no found any gift certificate based on id 
	 */
	@DeleteMapping(path="/{id:[0-9]+}", produces = "application/json")
	public ResponseUtils deleteGiftCertificate(@PathVariable long id) {
		return ResponseUtils.response(200, "Gift Certificate deleted successfully", giftCertManager.deleteGiftCertificateById(id));
	}
	
	/**
	 * Used to find gift certificate tags
	 * 
	 * @param {@link GiftCertificate} id 
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 */
	@GetMapping("/{giftCertId:[0-9]+}/tags")
	public ResponseUtils findGiftCertificateTags(@PathVariable long giftCertId) {
		return ResponseUtils.success(tagManager.findTagsByGiftCertificateId(giftCertId));
	}
}
