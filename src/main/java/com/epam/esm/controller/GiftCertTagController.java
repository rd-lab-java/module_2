package com.epam.esm.controller;

import java.awt.print.Pageable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
import com.epam.esm.service.GiftCertManager;
import com.epam.esm.service.TagManager;
import com.epam.esm.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.util.ResponseUtils;

/**
 * Rest controller api for managing 
 * gift certificate and tag based requests and responses. 
 * 
 * @author yusuf
 *
 */
@RestController
@RequestMapping("/giftcertsandtags")
public class GiftCertTagController {

	/** Gift Certificate service class variable*/
	@Autowired 
	private GiftCertManager giftCertManager;
	
	/**
	 * Find gift certificates and tags based on specific parameters,
	 * by tagName, part of name or part of description and also can be sorted 
	 * by date or name
	 * 
	 * @param tagName	 Name of tag that gift certificates belong to
	 * @param partOfName Part of name that is common to some gift certificates name
	 * @param partOfDesc Part of description that is common to some gift certificate description
	 * @param sortBy 	sort parameters, such as date, name;ASC or name;DESC
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and list of actual data ({@link GiftCertificate})
	 * @throws Not found exception {@link GiftCertificateNotFoundException},
	 * when no any data based on the provided parameters
	 */
	@GetMapping
	public ResponseUtils getGiftCertsWithTags(@RequestParam(name = "tag_name", required = false) String tagName, 
														@RequestParam(name = "part_of_name", required = false) String partOfName,
														@RequestParam(name = "part_of_desc", required = false) String partOfDesc,
														@RequestParam(name = "sort", required = false) String[] sortBy) {
		if(tagName != null) {
			if(sortBy == null)
				return ResponseUtils.success(giftCertManager.findGiftCertificatesAndTagsByTagName(tagName));
			return ResponseUtils.success(giftCertManager.findSortedGiftCertificatesAndTagsByTagName(tagName, sortBy));
		} else if(partOfName != null) {
			if(sortBy == null)
				return ResponseUtils.success(giftCertManager.findGiftCertificatesAndTagsByPartOfName(partOfName));
			return ResponseUtils.success(giftCertManager.findSortedGiftCertificatesAndTagsByPartOfName(partOfName, sortBy));
		} else if(partOfDesc != null) {
			if(sortBy == null)
				return ResponseUtils.success(giftCertManager.findGiftCertificatesAndTagsByPartOfDesc(partOfDesc));
			return ResponseUtils.success(giftCertManager.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sortBy));
		}
		throw new IllegalStateException("Resource cannot be found with null arguments, "
				+ "provide tag name, part of name or part of description");
		
	}
}
