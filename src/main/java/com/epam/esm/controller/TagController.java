package com.epam.esm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
import com.epam.esm.service.TagManager;
import com.epam.esm.service.exception.TagDuplicateException;
import com.epam.esm.service.exception.TagNotFoundException;
import com.epam.esm.util.ResponseUtils;

/**
 * Rest controller api for managing 
 * tag based requests and responses.
 * 
 * @author yusuf
 *
 */
@RestController
@RequestMapping("/tags")
public class TagController {
	
	/** Tag manager service class variable */
	@Autowired
	private TagManager tagManager;
	
	/**
	 * Used to find all tags 
	 * 
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link Tag})
	 * @throws Not found exception {@link TagNotFoundException}
	 * when no any data
	 */
	@GetMapping(produces = "application/json")
	public ResponseUtils findAll() {
		return ResponseUtils.success(tagManager.findAll());
	}
	
	/**
	 * Used to save tag 
	 * 
	 * @param tag {@link Tag} to save
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link Tag})
	 * @throws {@link TagDuplicateException} when providing duplicate data
	 */
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseUtils saveTag(@RequestBody Tag tag) {
		return ResponseUtils.response(201, "Tag created successfully", tagManager.saveTag(tag));
	}
	
	/**
	 * Used to find tag by tag id
	 * 
	 * @param tagId 
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link Tag})
	 * @throws Not found exception {@link TagNotFoundException} 
	 *  when no any data based on tag id
	 */
	@GetMapping("/{tagId:[0-9]+}")
	public ResponseUtils findTag(@PathVariable long tagId) {
		return ResponseUtils.success(tagManager.findTagById(tagId));
	}
	
	/**
	 * Used to delete tag by tag id
	 * 
	 * @param tagId
	 * @return The common response {@link ResponsUtils} containing status code,
	 *  message and actual data ({@link Tag})
	 * @throws Not found exception {@link TagNotFoundException} 
	 *  when no any data based on tag id
	 */
	@DeleteMapping("/{tagId:[0-9]+}")
	public ResponseUtils deleteTag(@PathVariable long tagId) {
		return ResponseUtils.response(200, "Tag deleted successfully", tagManager.deleteTagById(tagId));
	}
}
