package com.epam.esm.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

import javax.sql.DataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.epam.esm.controller")
@PropertySources({
	@PropertySource(name="db_prod.properties", value = "classpath:db_prod.properties")
})
public class WebConfig implements WebMvcConfigurer {
	
	@Autowired
	private Environment environment;
	
    @Bean
    @Profile("prod")
    public DataSource getProdDataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(environment.getProperty("database.prod.classname"));
        basicDataSource.setUrl(environment.getProperty("database.prod.url"));
        basicDataSource.setMaxIdle(environment.getProperty("database.prod.maxIdle", Integer.class));
        basicDataSource.setMaxWaitMillis(environment.getProperty("database.prod.maxWait", Long.class));
        basicDataSource.setMaxTotal(environment.getProperty("database.prod.maxActive", Integer.class));
        basicDataSource.setUsername(environment.getProperty("database.prod.user"));
        basicDataSource.setPassword(environment.getProperty("database.prod.password"));
        return basicDataSource;
    }

    @Bean
    @Profile("prod")
    public PlatformTransactionManager txManager(DataSource dataSource) {
    	return new DataSourceTransactionManager(dataSource);
    }
    
    @Bean
    @Profile("prod")
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
