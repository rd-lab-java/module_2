package com.epam.esm.config;

import java.util.Arrays;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import com.epam.esm.repository.GiftCertRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.impl.GiftCertRepositoryImpl;
import com.epam.esm.repository.impl.TagRepositoryImpl;

@Configuration
@PropertySources({
	@PropertySource(name="sqlStatements", value = "classpath:sql.statements"),
	@PropertySource(name="stringValues", value = "classpath:string.values")
})
public class TestConfig {

	@Autowired
	private Environment environment;
	
	@Bean
	@Profile("dev")
	public DataSource getDevDataSource() {
		return new EmbeddedDatabaseBuilder()
				.setType(EmbeddedDatabaseType.H2)
				.addScript("classpath:schema.sql")
				.build();
	}
 	
 	@Bean
 	@Profile("dev")
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
 	
 	@Bean
 	@Profile("dev")
 	public TagRepository getTagRepository(Environment environment, JdbcTemplate jdbcTemplate) {
 		return new TagRepositoryImpl(jdbcTemplate, environment);
 	}
 	
 	@Bean
 	@Profile("dev")
 	public GiftCertRepository getGiftCertRepository(Environment environment, JdbcTemplate jdbcTemplate) {
 		return new GiftCertRepositoryImpl(environment, jdbcTemplate, getTagRepository(environment, jdbcTemplate));
 	}
}
