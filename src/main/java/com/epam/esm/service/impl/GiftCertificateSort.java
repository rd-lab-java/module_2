package com.epam.esm.service.impl;

import org.springframework.stereotype.Component;

import com.epam.esm.service.Sortable;

/**
 * The implementation of {@link Sortable} interface 
 * @author yusuf
 *
 */
@Component
public class GiftCertificateSort implements Sortable {

	@Override
	public String sort(String[] sortBy) {
		StringBuffer sort = new StringBuffer();
		for(String token: sortBy) {
			if(token.equals("date")) {
				if(sort.length() != 0) {
					sort.append(',');
				}
				sort.append("gift_certificate.create_date");
			}
			
			if(token.startsWith("name")){
				if(sort.length() != 0) {
					sort.append(',');
				}
				sort.append("gift_certificate.name ").append(token.split(";")[1]);
			}
		}
		return sort.toString();
	}
}
