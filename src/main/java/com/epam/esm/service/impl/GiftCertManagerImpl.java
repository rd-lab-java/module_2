package com.epam.esm.service.impl;

import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.esm.repository.GiftCertRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
import com.epam.esm.service.GiftCertManager;

/**
 * Gift Certificate Interface Implementations
 * that contains implementation of business logics. 
 * @author yusuf
 *
 */
@Service
public class GiftCertManagerImpl implements GiftCertManager {

	private GiftCertificateSort giftCertificateSort;
	private GiftCertRepository giftCertRepository;
	
	@Autowired
	public GiftCertManagerImpl(GiftCertificateSort giftCertificateSort, GiftCertRepository giftCertRepository) {
		this.giftCertificateSort = giftCertificateSort;
		this.giftCertRepository = giftCertRepository;
	}
	
	@Override
	@Transactional
	public GiftCertificate saveGiftCertificate(GiftCertificate giftCertificate) {
		return giftCertRepository.saveGiftCertificate(giftCertificate);
	}
	
	@Override
	@Transactional
	public List<GiftCertificate> findAll() {
		return giftCertRepository.findAll();
	}

	@Override
	@Transactional
	public GiftCertificate findGiftCertificateById(long id) {
		return giftCertRepository.findGiftCertificateById(id);
	}

	@Override
	@Transactional
	public GiftCertificate updateGiftCertificateById(long id, GiftCertificate giftCertificate) {
		return giftCertRepository.updateGiftCertificateById(id, giftCertificate);		
	}

	@Override
	@Transactional
	public GiftCertificate deleteGiftCertificateById(long id) {
		return giftCertRepository.deleteGiftCertificateById(id);
	}

	@Override
	@Transactional
	public List<GiftCertificate> findSortedGiftCertificatesAndTagsByTagName(String tagName, String[] sortBy) {
		return giftCertRepository.findSortedGiftCertificatesByTagName(tagName, giftCertificateSort.sort(sortBy));
	}
	
	@Override
	@Transactional
	public List<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfName(String partOfName, String[] sortBy) {
		return giftCertRepository.findSortedGiftCertificatesByPartOfName(partOfName, giftCertificateSort.sort(sortBy));
	}

	@Override
	@Transactional
	public List<GiftCertificate> findGiftCertificatesAndTagsByTagName(String tagName) {
		return giftCertRepository.findGiftCertificatesByTagName(tagName);
	}

	@Override
	@Transactional
	public List<GiftCertificate> findGiftCertificatesAndTagsByPartOfName(String partOfName) {
		return giftCertRepository.findGiftCertificatesByPartOfName(partOfName);
	}

	@Override
	@Transactional
	public List<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfDesc(String partOfDesc, String[] sortBy) {
		return giftCertRepository.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, giftCertificateSort.sort(sortBy));
	}

	@Override
	@Transactional
	public List<GiftCertificate> findGiftCertificatesAndTagsByPartOfDesc(String partOfDesc) {
		return giftCertRepository.findGiftCertificatesByPartOfDesc(partOfDesc);
	}
}
