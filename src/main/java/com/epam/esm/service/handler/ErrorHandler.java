package com.epam.esm.service.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.epam.esm.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.service.exception.GiftCertificateNotModifiedException;
import com.epam.esm.service.exception.TagDuplicateException;
import com.epam.esm.service.exception.TagNotFoundException;
import com.epam.esm.util.ResponseUtils;

@ControllerAdvice
@RestController
@PropertySource(name="error_code.values", value = "classpath:error_code.values")
public class ErrorHandler {
	
	@Autowired 
	private Environment environment;
	
	@ExceptionHandler(GiftCertificateNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseUtils giftCertNotFoundExceptionHandler(GiftCertificateNotFoundException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("giftcertificate.not_found", Integer.class), ex.getMessage(), null);
	}
	
	@ExceptionHandler(GiftCertificateDuplicateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils giftCertDuplicateExceptionHandler(GiftCertificateDuplicateException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("giftcertificate.internal_server_error", Integer.class), ex.getMessage(), null);
	}
	
	@ExceptionHandler(GiftCertificateNotModifiedException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils giftCertNotModifiedExceptionHandler(GiftCertificateNotModifiedException ex, WebRequest request) {
		System.out.println("Not modified exception thrown");
		return ResponseUtils.response(environment.getProperty("giftcertificate.internal_server_error", Integer.class), ex.getMessage(), null);
	}
	
	@ExceptionHandler(TagNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseUtils giftCertTagsNotFoundExceptionHandler(TagNotFoundException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("tag.not_found", Integer.class), ex.getMessage(), null);
	}
	
	@ExceptionHandler(TagDuplicateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils tagDuplicateExceptionHandler(TagDuplicateException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("tag.internal_server_error", Integer.class), ex.getMessage(), null);
	}
	
	@ExceptionHandler(IllegalStateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseUtils illeageStateExceptionHandler(IllegalStateException ex, WebRequest request) {
		return ResponseUtils.response(environment.getProperty("giftcertificate.internal_server_error", Integer.class), ex.getMessage(), null);
	}
}
