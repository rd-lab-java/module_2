package com.epam.esm.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_MODIFIED, reason = "Gift Certificate content not modified")
public class GiftCertificateNotModifiedException extends RuntimeException {

	private static final long serialVersionUID = 2953397588642198502L;

	public GiftCertificateNotModifiedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateNotModifiedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateNotModifiedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateNotModifiedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateNotModifiedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
