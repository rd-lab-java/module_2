package com.epam.esm.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Gift Certificate Not Found")
public class GiftCertificateNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1575953473563838894L;
	
	public GiftCertificateNotFoundException() {
		super();
	}

	public GiftCertificateNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
