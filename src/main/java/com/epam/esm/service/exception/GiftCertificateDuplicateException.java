package com.epam.esm.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Gift Certificate Duplicate Exception")
public class GiftCertificateDuplicateException extends RuntimeException {

	private static final long serialVersionUID = 6315049771712745727L;

	public GiftCertificateDuplicateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateDuplicateException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateDuplicateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateDuplicateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GiftCertificateDuplicateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
