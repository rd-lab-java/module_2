package com.epam.esm.service;

import java.util.List;

import com.epam.esm.repository.entity.Tag;
/**
 * This interface imposes CRUD operations business logic on tag object
 * @author yusuf
 *
 */
public interface TagManager {
	Tag saveTag(Tag tag);
	List<Tag> findTagsByGiftCertificateId(long giftCertId);
	List<Tag> findAll();
	Tag deleteTagById(long id);
	Tag findTagById(long tagId);
}
