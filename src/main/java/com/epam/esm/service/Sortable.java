package com.epam.esm.service;
/**
 * This interface imposes converting on giving array of string to 
 * gift certificate field based parameters string description.
 * @author yusuf
 *
 */
public interface Sortable {
	/**
	 * Converts array of specific parameters to 
	 * gift certificate field based query description 
	 * 
	 * @param sortBy array of string
	 * @return String based on gift certificate fields
	 */
	String sort(String[] sortBy);
}
