package service;

import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doReturn;
import static org.mockito.ArgumentMatchers.any;

import com.epam.esm.config.TestConfig;
import com.epam.esm.repository.GiftCertRepository;
import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
import com.epam.esm.service.GiftCertManager;
import com.epam.esm.service.Sortable;
import com.epam.esm.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.service.exception.GiftCertificateNotModifiedException;
import com.epam.esm.service.impl.GiftCertManagerImpl;
import com.epam.esm.service.impl.GiftCertificateSort;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes =  {TestConfig.class})
@ActiveProfiles("dev")
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
public class GiftCertificateManagerTest {
	
	GiftCertManager giftCertManager;
	
	@Autowired
	GiftCertRepository giftCertRepository;
	
	@Mock
	GiftCertificateSort giftCertificateSort;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		giftCertManager = new GiftCertManagerImpl(giftCertificateSort, giftCertRepository);
		
		
		GiftCertificate gc1 = new GiftCertificate();
		gc1.setName("GC-one");
		gc1.setDescription("gift certificate one for integration test");
		gc1.setPrice(10.5);
		gc1.setDuration(10);
		Tag tag1 = new Tag();
		tag1.setName("dell");
		gc1.setTags(Arrays.asList(tag1));
		
		GiftCertificate gc2 = new GiftCertificate();
		gc2.setName("GC-two");
		gc2.setDescription("gift certificate two for integration test");
		gc2.setPrice(12.5);
		gc2.setDuration(20);
		Tag tag2 = new Tag();
		tag2.setName("lenovo");
		gc2.setTags(Arrays.asList(tag1, tag2));
		
		GiftCertificate gc3 = new GiftCertificate();
		gc3.setName("GC-three");
		gc3.setDescription("gift certificate three for integration test");
		gc3.setPrice(13.5);
		gc3.setDuration(30);
		Tag tag3 = new Tag();
		tag3.setName("asus");
		gc3.setTags(Arrays.asList(tag1, tag2, tag3));
		
		giftCertRepository.saveGiftCertificate(gc1);
		giftCertRepository.saveGiftCertificate(gc2);
		giftCertRepository.saveGiftCertificate(gc3);
	}
	
	
	@AfterEach
	public void tearDown() {
		giftCertRepository.deleteAll();
	}

	@Test
	@DisplayName("Find sorted gift certificates and tags by tag name")
	public void shouldFindGiftCertificatesAndTagsWithDateSortByTagName() {
		when(giftCertificateSort.sort(new String[] {"date"})).thenReturn("gift_certificate.create_date");
		String tagName = "lenovo";
		String[] sortBy = {"date"};
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByTagName(tagName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(2, giftCertificates.size());
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by tag name")
	public void shouldFindGiftCertificatesAndTagsWithNameASCSortByTagName() {
		String tagName = "lenovo";
		String[] sortBy = {"name;ASC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.name ASC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByTagName(tagName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(2, giftCertificates.size());
		assertTrue("gc-three".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-two".equalsIgnoreCase(giftCertificates.get(1).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by tag name")
	public void shouldFindGiftCertificatesAndTagsWithNameDESCSortByTagName() {
		String tagName = "lenovo";
		String[] sortBy = {"name;DESC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.name DESC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByTagName(tagName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(2, giftCertificates.size());
		assertTrue("gc-two".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by tag name")
	public void shouldFindGiftCertificatesAndTagsWithDateAndNameDESCSortByTagName() {
		String tagName = "lenovo";
		String[] sortBy = {"date", "name;DESC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByTagName(tagName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(2, giftCertificates.size());
		assertTrue("gc-two".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by tag name")
	public void shouldFindGiftCertificatesAndTagsWithDateAndNameASCSortByTagName() {
		String tagName = "lenovo";
		String[] sortBy = {"date", "name;ASC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.create_date, gift_certificate.name ASC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByTagName(tagName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(2, giftCertificates.size());
		assertTrue("gc-two".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Throw exception when find sorted gift certificates and tags by tag name")
	public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsWithDateAndNameDESCSortByTagName() {
		String tagName = "nelly";
		String[] sortBy = {"date", "name;DESC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
		String errorMessage = "Searching resources not found.";
		GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () ->{
			giftCertManager.findSortedGiftCertificatesAndTagsByTagName(tagName, sortBy);
		});
		assertEquals(errorMessage, ex.getMessage());
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of name")
	public void shouldFindGiftCertificatesAndTagsWithDateSortByPartOfName() {
		when(giftCertificateSort.sort(new String[] {"date"})).thenReturn("gift_certificate.create_date");
		String partOfName = "GC";
		String[] sortBy = {"date"};
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfName(partOfName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of name")
	public void shouldFindGiftCertificatesAndTagsWithNameASCSortByPartOfName() {
		String partOfName = "GC";
		String[] sortBy = {"name;ASC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.name ASC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfName(partOfName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		assertTrue("gc-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName()) && "gc-two".equalsIgnoreCase(giftCertificates.get(2).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of name")
	public void shouldFindGiftCertificatesAndTagsWithNameDESCSortByPartOfName() {
		String partOfName = "GC";
		String[] sortBy = {"name;DESC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.name DESC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfName(partOfName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		assertTrue("gc-two".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName()) &&
				"gc-one".equalsIgnoreCase(giftCertificates.get(2).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of name")
	public void shouldFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfName() {
		String partOfName = "GC";
		String[] sortBy = {"date", "name;DESC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfName(partOfName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		assertTrue("gc-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-two".equalsIgnoreCase(giftCertificates.get(1).getName()) &&
				"gc-three".equalsIgnoreCase(giftCertificates.get(2).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of name")
	public void shouldFindGiftCertificatesAndTagsWithDateAndNameASCSortByPartOfName() {
		String partOfName = "GC";
		String[] sortBy = {"date", "name;ASC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.create_date, gift_certificate.name ASC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfName(partOfName, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		assertTrue("gc-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-two".equalsIgnoreCase(giftCertificates.get(1).getName()) 
				&& "gc-three".equalsIgnoreCase(giftCertificates.get(2).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Throw exception when find sorted gift certificates and tags by tag name")
	public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfName() {
		String partOfName = "nelly";
		String[] sortBy = {"date", "name;DESC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
		String errorMessage = "Searching resources not found.";
		GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () ->{
			giftCertManager.findSortedGiftCertificatesAndTagsByPartOfName(partOfName, sortBy);
		});
		assertEquals(errorMessage, ex.getMessage());
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of desc")
	public void shouldFindGiftCertificatesAndTagsWithDateSortByPartOfDesc() {
		when(giftCertificateSort.sort(new String[] {"date"})).thenReturn("gift_certificate.create_date");
		String partOfDesc = "certificate";
		String[] sortBy = {"date"};
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of desc")
	public void shouldFindGiftCertificatesAndTagsWithNameASCSortByPartOfDesc() {
		String partOfDesc = "certificate";
		String[] sortBy = {"name;ASC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.name ASC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		assertTrue("gc-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName()) && "gc-two".equalsIgnoreCase(giftCertificates.get(2).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of desc")
	public void shouldFindGiftCertificatesAndTagsWithNameDESCSortByPartOfDesc() {
		String partOfDesc = "certificate";
		String[] sortBy = {"name;DESC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.name DESC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		assertTrue("gc-two".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName()) &&
				"gc-one".equalsIgnoreCase(giftCertificates.get(2).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of desc")
	public void shouldFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfDesc() {
		String partOfDesc = "certificate";
		String[] sortBy = {"date", "name;DESC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		assertTrue("gc-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-two".equalsIgnoreCase(giftCertificates.get(1).getName()) &&
				"gc-three".equalsIgnoreCase(giftCertificates.get(2).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of desc")
	public void shouldFindGiftCertificatesAndTagsWithDateAndNameASCSortByPartOfDesc() {
		String partOfDesc = "certificate";
		String[] sortBy = {"date", "name;ASC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.create_date, gift_certificate.name ASC");
		List<GiftCertificate> giftCertificates = giftCertManager.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sortBy);
		assertNotNull(giftCertificates);
		assertEquals(3, giftCertificates.size());
		assertTrue("gc-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-two".equalsIgnoreCase(giftCertificates.get(1).getName()) 
				&& "gc-three".equalsIgnoreCase(giftCertificates.get(2).getName()));
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
	
	@Test
	@DisplayName("Find sorted gift certificates and tags by part of desc")
	public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfDesc() {
		String partOfDesc = "bottle";
		String[] sortBy = {"date", "name;DESC"};
		when(giftCertificateSort.sort(sortBy)).thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
		String errorMessage = "Searching resources not found.";
		GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () ->{
			giftCertManager.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sortBy);
		});
		assertEquals(errorMessage, ex.getMessage());
		verify(giftCertificateSort, VerificationModeFactory.times(1)).sort(sortBy);
	}
}
