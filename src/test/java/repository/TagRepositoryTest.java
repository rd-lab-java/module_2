package repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.esm.config.TestConfig;
import com.epam.esm.repository.GiftCertRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
import com.epam.esm.service.exception.TagDuplicateException;
import com.epam.esm.service.exception.TagNotFoundException;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes =  {TestConfig.class})
@ActiveProfiles("dev")
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
public class TagRepositoryTest {
	
	@Autowired
	GiftCertRepository giftCertRepository;

	@Autowired
	TagRepository tagRepository;
	
	@AfterEach
	public void tearDown() {
		giftCertRepository.deleteAll();
	}
	
	@BeforeEach
	public void init() {
		Tag tagOne = new Tag();
		tagOne.setName("adorable");
		
		Tag tagTwo = new Tag();
		tagTwo.setName("rainy");
		
		Tag tagThree = new Tag();
		tagThree.setName("sunny");

		tagRepository.saveTag(tagTwo);
		tagRepository.saveTag(tagThree);
		tagRepository.saveTag(tagOne);
	}
	
	@Test
	@DisplayName("Save tag")
	public void shouldSaveTag() {
		Tag tagFour = new Tag();
		tagFour.setName("snowy");
		
		Tag savedTagFour = tagRepository.saveTag(tagFour);
		assertNotNull(savedTagFour);
		assertEquals(tagFour.getName(), savedTagFour.getName());
	}
	
	@Test
	@DisplayName("Throw duplicate exception when save tag")
	public void shouldThrowExceptionWhenSaveTag() {
		Tag tag = new Tag();
		tag.setName("rainy");
		String errorMessage = "Tag with 'rainy' name already existed.";
		TagDuplicateException ex = assertThrows(TagDuplicateException.class, () -> {
			tagRepository.saveTag(tag);
		});
		
		assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Find tag by name")
	public void shouldFindTagByName() {
		Tag foundTag = tagRepository.findTagByName("rainy");
		assertNotNull(foundTag);
		assertEquals("rainy", foundTag.getName());
	}
	
	@Test
	@DisplayName("Throw not found exception when find tag by name")
	public void shouldThrowNotFoundExceptionWhenFindTagByName() {
		String errorMessage = "Requested resource not found by (name = geeks).";
		TagNotFoundException ex = assertThrows(TagNotFoundException.class, () -> {
			tagRepository.findTagByName("geeks");
		});
		assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Find tags by certificate id")
	public void shouldFindTagsByGiftCertificateId() {
		GiftCertificate gcOne = new GiftCertificate();
		gcOne.setName("GC-one");
		gcOne.setDescription("gift certificate one for integration test");
		gcOne.setPrice(10.5);
		gcOne.setDuration(10);
		
		Tag tagOne = tagRepository.findTagByName("rainy");
		Tag tagTwo = tagRepository.findTagByName("adorable");
		gcOne.setTags(Arrays.asList(tagOne, tagTwo));
		GiftCertificate savedGiftCertificate = giftCertRepository.saveGiftCertificate(gcOne);
		List<Tag> tags = tagRepository.findTagsByGiftCertificateId(savedGiftCertificate.getId());
		assertNotNull(tags);
		assertEquals(2, tags.size());
	}
	
	@Test
	@DisplayName("Find all tags")
	public void shouldFindAllTags() {
		List<Tag> allTags = tagRepository.findAll();
		assertNotNull(allTags);
		assertEquals(3, allTags.size());
	}
	
	@Nested
	class innerClass {
		@BeforeEach 
		public void init() {
			giftCertRepository.deleteAll();
		}
		
		@Test
		@DisplayName("Throw not found exception when find all tags")
		public void shouldthrowNotFoundExceptionWhenFindAll() {
			String errorMessage = "Requested resources not found.";
			TagNotFoundException ex = assertThrows(TagNotFoundException.class, () ->{
				tagRepository.findAll();
			});
			
			assertEquals(errorMessage, ex.getMessage());
		}
	}
	
	@Test
	@DisplayName("Delete tag by id")
	public void shouldDeleteTagById() {
		Tag deletedTag = tagRepository.deleteTagById(2);
		assertNotNull(deletedTag);
		
		String errorMessage = "Requested resource not found by (id = 2).";
		TagNotFoundException ex = assertThrows(TagNotFoundException.class, ()-> {
			tagRepository.deleteTagById(deletedTag.getId());
		});
		assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Throw tag not found exception when delete tag by id")
	public void shouldThrowNotFoundException() {
		String errorMessage = "Requested resource not found by (id = 5).";
		TagNotFoundException ex = assertThrows(TagNotFoundException.class, ()-> {
			tagRepository.deleteTagById(5);
		});
		assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Find tag by id")
	public void shouldFindTagById() {
		Tag tag = tagRepository.findTagById(3);
		assertNotNull(tag);
		assertEquals(3, tag.getId());
		assertEquals("adorable", tag.getName());
	}
	
	@Test
	@DisplayName("Throw tag not found exception when find tag by id")
	public void shouldThrowNotFoundExceptionWhenFindTagById() {
		String errorMessage = "Requested resource not found by (id = 10).";
		TagNotFoundException ex = assertThrows(TagNotFoundException.class, ()-> {
			tagRepository.findTagById(10);
		});
		
		assertEquals(errorMessage, ex.getMessage());
	}
}
