package repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.After;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.objenesis.instantiator.gcj.GCJInstantiator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import com.epam.esm.config.RestBasicsInitializer;
import com.epam.esm.config.RootConfig;
import com.epam.esm.config.TestConfig;
import com.epam.esm.config.WebConfig;
import com.epam.esm.repository.GiftCertRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificate;
import com.epam.esm.repository.entity.Tag;
import com.epam.esm.repository.impl.GiftCertRepositoryImpl;
import com.epam.esm.repository.impl.TagRepositoryImpl;
import com.epam.esm.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.service.exception.GiftCertificateNotModifiedException;
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes =  {TestConfig.class})
@ActiveProfiles("dev")
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
public class GiftCertificateRepositoryTest {

	@Autowired
	GiftCertRepository giftCertRepository;
	
	@Autowired
	TagRepository tagRepository;
	
	@BeforeEach
	public void init() {
		GiftCertificate gc1 = new GiftCertificate();
		gc1.setName("GC-one");
		gc1.setDescription("gift certificate one for integration test");
		gc1.setPrice(10.5);
		gc1.setDuration(10);
		Tag tag1 = new Tag();
		tag1.setName("dell");
		gc1.setTags(Arrays.asList(tag1));
		
		
		GiftCertificate gc2 = new GiftCertificate();
		gc2.setName("GC-two");
		gc2.setDescription("gift certificate two for integration test");
		gc2.setPrice(12.5);
		gc2.setDuration(20);
		Tag tag2 = new Tag();
		tag2.setName("lenovo");
		gc2.setTags(Arrays.asList(tag1, tag2));
		
		GiftCertificate gc3 = new GiftCertificate();
		gc3.setName("GC-three");
		gc3.setDescription("gift certificate three for integration test");
		gc3.setPrice(13.5);
		gc3.setDuration(30);
		Tag tag3 = new Tag();
		tag3.setName("asus");
		gc3.setTags(Arrays.asList(tag1, tag2, tag3));
		
		giftCertRepository.saveGiftCertificate(gc1);		
		giftCertRepository.saveGiftCertificate(gc2);
		giftCertRepository.saveGiftCertificate(gc3);
	}
	
	@AfterEach
	public void tearDown() {
		giftCertRepository.deleteAll();
	}
	
	@Test
	@DisplayName("Save gift certificate")
	public void shouldSaveGiftCertificate() {
		GiftCertificate newGC = new GiftCertificate();
		newGC.setName("GC-four");
		newGC.setDescription("gift certificate four for integration test");
		newGC.setPrice(14.5);
		newGC.setDuration(40);
		Tag tag1 = new Tag();
		tag1.setName("apple");
		newGC.setTags(Arrays.asList(tag1));
		
		GiftCertificate saveGC1 = giftCertRepository.saveGiftCertificate(newGC);
		Assertions.assertNotNull(saveGC1);
		Assertions.assertEquals(1, saveGC1.getTags().size());
		
	}
	
//	@Order(2)
	@Test
	@DisplayName("throw duplicate exception when inserting a data with the same name")
	public void shouldThrowExceptionWhenSaveGiftCertificate() {
		String errorMessage = "Gift Certificate with 'GC-one' name already existed.";
		
		GiftCertificate giftCertificate = new GiftCertificate();
		giftCertificate.setName("GC-one");
		giftCertificate.setDescription("gift certificate one for integration test");
		giftCertificate.setPrice(10.5);
		giftCertificate.setDuration(10);
		Tag tag = new Tag();
		tag.setName("lenovo");
		giftCertificate.setTags(Arrays.asList(tag));
		
		GiftCertificateDuplicateException ex = assertThrows(GiftCertificateDuplicateException.class, () -> {
			giftCertRepository.saveGiftCertificate(giftCertificate);
		});
		
		assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Find all gift certificates")
	public void shouldFindAllGiftCertificates() {
		List<GiftCertificate> giftCertificates = giftCertRepository.findAll();
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(3, giftCertificates.size());
	}
	
	@Nested
	class InnerClassOne {
		@BeforeEach 
		public void init() {
			giftCertRepository.deleteAll();
		}
		
		@Test
		@DisplayName("Should throw exception when find all")
		public void shouldThrowExceptionWhenFindAll() {
			String errorMessage = "Requested resources not found.";
			GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
				giftCertRepository.findAll();
			});
			
			assertEquals(errorMessage, ex.getMessage());
		}
	}
	
	@Test
	@DisplayName("Find gift certificate by id")
	public void shouldFindGiftCertificateById() {
		GiftCertificate giftCertificate = giftCertRepository.findGiftCertificateById(2L);
		assertNotNull(giftCertificate, "GiftCertificate should not be null");
		assertEquals(2L, giftCertificate.getId());
		assertEquals("GC-two", giftCertificate.getName());
		assertEquals("gift certificate two for integration test", giftCertificate.getDescription());
		assertEquals(12.5, giftCertificate.getPrice());
		assertEquals(20, giftCertificate.getDuration());
	}
	
	@Test
	@DisplayName("Throw Exception when find gift certificate by id")
	public void shouldThrowExceptionWhenGiftCertificateById() {
		String errorMessage = "Requested resource not found (id = 19).";
		GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.findGiftCertificateById(19L);
		});
		assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Update gift certificate by id")
	public void shouldUpdateGiftCertificateById() {
		GiftCertificate gc1 = giftCertRepository.findGiftCertificateById(1);
		gc1.setName("updated gc1");
		gc1.setDescription("updated gift certificate by id in integration test");
		Tag tag = new Tag();
		tag.setName("acer");
		gc1.setTags(Arrays.asList(tag));
		
		GiftCertificate updatedGC = giftCertRepository.updateGiftCertificateById(1, gc1);
		Assertions.assertNotNull(updatedGC, "Updated gift certificate should be not null");
		Assertions.assertEquals(gc1.getName(), updatedGC.getName());
		Assertions.assertEquals(gc1.getDescription(), updatedGC.getDescription());
		Assertions.assertEquals(tag.getName(), ((List<Tag>) updatedGC.getTags()).get(0).getName());
	}
	
	@Test
	@DisplayName("Update gift certificate by adding tag by id")
	public void shouldUpdateGiftCertificateWithTagById() {
		GiftCertificate gc1 = giftCertRepository.findGiftCertificateById(1);
		Tag tag = new Tag();
		tag.setName("asus");
		gc1.setTags(Arrays.asList(tag));
		
		GiftCertificate updatedGC = giftCertRepository.updateGiftCertificateById(1, gc1);
		Assertions.assertNotNull(updatedGC, "Updated gift certificate should be not null");
		Assertions.assertEquals(tag.getName(), ((List<Tag>) updatedGC.getTags()).get(0).getName());
	}
	
	@Test
	@DisplayName("Throw not modified exception when update gift certificate by id")
	public void shouldThrowGiftCertificateNotModifiedException() {
		String errorMessage = "Gift Certificate with 1 id not modified.";
		GiftCertificate gc1 = giftCertRepository.findGiftCertificateById(1L);
		GiftCertificateNotModifiedException ex = Assertions.assertThrows(GiftCertificateNotModifiedException.class, () -> {
			giftCertRepository.updateGiftCertificateById(1L,  gc1);
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Throw not found exception when update gift certificate by id")
	public void shouldThrowGiftCertificateNotFoundException() {
		String errorMessage = "Requested resource not found (id = 5).";
		GiftCertificate gc1 = giftCertRepository.findGiftCertificateById(1L);
		gc1.setName("new name for gc1");
		GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.updateGiftCertificateById(5L,  gc1);
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Delete gift certificate by id")
	public void shouldDeleteGiftCertificateById() {
		GiftCertificate gc = giftCertRepository.deleteGiftCertificateById(2L);
		Assertions.assertNotNull(gc);
		
		String errorMessage = "Requested resource not found (id = 2).";
		GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.findGiftCertificateById(gc.getId());
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Throw exception when delete gift certificate by id")
	public void shouldThrowExceptionWhenDeleteGiftCertificateById() {		

		String errorMessage = "Requested resource not found (id = 1).";
		giftCertRepository.deleteGiftCertificateById(1L);
		GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.deleteGiftCertificateById(1L);
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Save gift certificate and tag")
	public void shouldSaveGiftCertificateAndTag() {
		GiftCertificate giftCertificate = new GiftCertificate();
		giftCertificate.setName("gc-four");
		giftCertificate.setDescription("Gift certificate number 4");
		giftCertificate.setPrice(14.5);
		giftCertificate.setDuration(40);
		
		GiftCertificate savedGiftCertificate = giftCertRepository.saveGiftCertificate(giftCertificate);
		Assertions.assertNotNull(savedGiftCertificate, "Saved gift repository should not be null");

		Tag foundTag = tagRepository.findTagByName("lenovo");
		Assertions.assertNotNull(foundTag, "Saved tag should not be null");
		
		Assertions.assertTrue(giftCertRepository.saveGiftCertificateAndTag(savedGiftCertificate, foundTag));
	}
	
	@Test
	@DisplayName("Find gift certificates and tags by tag name and sorted with date")
	public void shouldFindGiftCertificatesAndTagsByTagNameAndSortByDate() {
		String sort = "gift_certificate.create_date";
		String tagName = "dell";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findSortedGiftCertificatesByTagName(tagName, sort);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(3, giftCertificates.size());
		Assertions.assertTrue("gc-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "GC-two".equalsIgnoreCase(giftCertificates.get(1).getName())
				&& "GC-three".equalsIgnoreCase(giftCertificates.get(2).getName()));
		
	}
	
	@Test
	@DisplayName("Find gift certificates and tags by tag name and sorted with name ASC")
	public void shouldFindGiftCertificatesAndTagsByTagNameAndSortByName() {
		String sort = "gift_certificate.name ASC";
		String tagName = "dell";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findSortedGiftCertificatesByTagName(tagName, sort);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(3, giftCertificates.size());
		Assertions.assertTrue("GC-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName()) && 
				"gc-two".equalsIgnoreCase(giftCertificates.get(2).getName()));
		
	}
	
	@Test
	@DisplayName("Find gift certificates and tags by tag name and sorted with date and name DESC")
	public void shouldFindGiftCertificatesAndTagsByTagNameAndSortByDateAndName() {
		String sort = "gift_certificate.create_date, gift_certificate.name DESC";
		String tagName = "lenovo";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findSortedGiftCertificatesByTagName(tagName, sort);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(2, giftCertificates.size());
		Assertions.assertTrue("GC-two".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName()));
		
	}
	
	@Test
	@DisplayName("Throw exception when find sorted gift certificates and tags by tagName") 
	public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByTagName() {
		String sort = "gift_certificate.create_date, gift_certificate.name DESC";
		String tagName = "woody";
		
		String errorMessage = "Searching resources not found.";
		GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.findSortedGiftCertificatesByTagName(tagName, sort);
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Find GiftCertificates and tags by tagName")
	public void shouldFindGiftCertificatesAndTagsByTagName() {
		String tagName = "asus";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findGiftCertificatesByTagName(tagName);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(1, giftCertificates.size());
		Assertions.assertTrue("gc-three".equalsIgnoreCase(giftCertificates.get(0).getName()));
	}
	
	@Test
	@DisplayName("Throw exception when find gift certificates and tags")
	public void shouldThrowExeptionWhenFindGiftCertificatesByTagName() {
		String tagName = "woody";
		String errorMessage = "Searching resources not found.";
		GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.findGiftCertificatesByTagName(tagName);
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Find sorted gift certificates by part of name sorted by date")
	public void shouldFindGiftCertificatesAndTagsByPartOfNameAndSortByDate() {
		String sort = "gift_certificate.create_date";
		String partOfName = "GC";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findSortedGiftCertificatesByPartOfName(partOfName, sort);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(3, giftCertificates.size());
		Assertions.assertTrue("GC-one".equalsIgnoreCase(giftCertificates.get(0).getName()) &&
				"gc-two".equalsIgnoreCase(giftCertificates.get(1).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(2).getName()));
		
	}
	
	@Test
	@DisplayName("Find gift certificates and tags by part of name and sorted with name ASC")
	public void shouldFindGiftCertificatesAndTagsByPartOfNameAndSortByName() {
		String sort = "gift_certificate.name ASC";
		String partOfName = "GC";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findSortedGiftCertificatesByPartOfName(partOfName, sort);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(3, giftCertificates.size());
		Assertions.assertTrue("gc-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-three".equalsIgnoreCase(giftCertificates.get(1).getName())
				&& "gc-two".equalsIgnoreCase(giftCertificates.get(2).getName()));
		
	}
	
	@Test
	@DisplayName("Throw exception when find sorted gift certificates and tags by part of name") 
	public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfName() {
		String sort = "gift_certificate.create_date, gift_certificate.name DESC";
		String partOfName = "moon";
		
		String errorMessage = "Searching resources not found.";
		GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.findSortedGiftCertificatesByPartOfName(partOfName, sort);
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Find GiftCertificates and tags by part of name")
	public void shouldFindGiftCertificatesAndTagsByPartOfName() {
		String partOfName = "-";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findGiftCertificatesByPartOfName(partOfName);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(3, giftCertificates.size());
		Assertions.assertTrue("GC-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "GC-two".equalsIgnoreCase(giftCertificates.get(1).getName()) && 
				"gc-three".equalsIgnoreCase(giftCertificates.get(2).getName()));
	}
	
	@Test
	@DisplayName("Throw exception when find gift certificates and tags by part of name")
	public void shouldThrowExeptionWhenFindGiftCertificatesByPartOfName() {
		String partOfName = "&?";
		String errorMessage = "Searching resources not found.";
		GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.findGiftCertificatesByPartOfName(partOfName);
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Find sorted gift certificates by part of desc sorted by date")
	public void shouldFindGiftCertificatesAndTagsByPartOfDescAndSortByDate() {
		String sort = "gift_certificate.create_date";
		String partOfDesc = "certificate";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sort);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(3, giftCertificates.size());
		Assertions.assertTrue("gc-two".equalsIgnoreCase(giftCertificates.get(1).getName()) && "GC-three".equalsIgnoreCase(giftCertificates.get(2).getName()) &&
				"GC-one".equalsIgnoreCase(giftCertificates.get(0).getName()));
		
	}
	
	@Test
	@DisplayName("Find gift certificates and tags by part of desc and sorted with name ASC")
	public void shouldFindGiftCertificatesAndTagsByPartOfDescAndSortByName() {
		String sort = "gift_certificate.name ASC";
		String partOfDesc = "integration";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sort);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(3, giftCertificates.size());
		Assertions.assertTrue("GC-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "GC-three".equalsIgnoreCase(giftCertificates.get(1).getName()) && "gc-two".equalsIgnoreCase(giftCertificates.get(2).getName()));
		
	}
	
	@Test
	@DisplayName("Throw exception when find sorted gift certificates and tags by part of desc") 
	public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfDesc() {
		String sort = "gift_certificate.create_date, gift_certificate.name DESC";
		String partOfDesc = "electric adapter";
		
		String errorMessage = "Searching resources not found.";
		GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sort);
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
	
	@Test
	@DisplayName("Find GiftCertificates and tags by part of desc")
	public void shouldFindGiftCertificatesAndTagsByPartOfDesc() {
		String partOfDesc = "test";
		
		List<GiftCertificate> giftCertificates = giftCertRepository.findGiftCertificatesByPartOfDesc(partOfDesc);
		
		Assertions.assertNotNull(giftCertificates);
		Assertions.assertEquals(3, giftCertificates.size());
		Assertions.assertTrue("gc-one".equalsIgnoreCase(giftCertificates.get(0).getName()) && "gc-two".equalsIgnoreCase(giftCertificates.get(1).getName()) && 
				"gc-three".equalsIgnoreCase(giftCertificates.get(2).getName()));
	}
	
	@Test
	@DisplayName("Throw exception when find gift certificates and tags by part of desc")
	public void shouldThrowExeptionWhenFindGiftCertificatesByPartOfDesc() {
		String partOfDesc = "trump and clinton";
		String errorMessage = "Searching resources not found.";
		GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
			giftCertRepository.findGiftCertificatesByPartOfDesc(partOfDesc);
		});
		
		Assertions.assertEquals(errorMessage, ex.getMessage());
	}
}
